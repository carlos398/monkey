import { NavLink } from "react-router-dom"
import "./Navbar.css"

const Navbar = () => {

  const ActiveLink = ({isActive}) => (isActive ? "Active": "")

  return (
    <div className='NavbarContainer'>
        <nav>
          <ul>
              <li>
                  <NavLink to='/' className={ActiveLink}>
                    <i class="fa-solid fa-house-user"></i>
                  </NavLink>
              </li>
              <li>
                  <NavLink to='/about' className={ActiveLink}>
                    <i class="fa-solid fa-money-bill"></i>
                  </NavLink>
              </li>
              <li>
                  <NavLink to='/contact' className={ActiveLink}>
                    <i class="fa-solid fa-address-card"></i>
                  </NavLink>
              </li>
          </ul>
        </nav>
    </div>
  )
}

export default Navbar