import "./Card.css"

const Card = ({ data }) => {

    const expendtype = data.type === "Gasto" ? "CardHeader gasto" : "CardHeader ingreso";

    return (
        <div className="Card">
            <div className={expendtype}>
                <h2>{data.type}</h2>
            </div>
            <div className="CardBody">
                <div className="CardImg">
                    <img src="" alt="" />
                </div>
                <div className="CardText">
                    <h2>{data.class}</h2>
                    <h3>$ {data.amount}</h3>
                    <h4>{data.date}</h4>
                </div>
            </div>
        </div>
    )
}

export default Card