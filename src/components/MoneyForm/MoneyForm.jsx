<div className="form-container">
    <form>
        <div className="form-group">
            <label htmlFor="Tipo">Tipo de gasto: </label>
            <select name="Tipo" id="Tipo">
                <option value="0">Select...</option>
                <option value="1">Ingreso</option>
                <option value="2">Gasto</option>
            </select>
        </div>
        <div className="form-group">
            <label htmlFor="Cantidad">Cantidad en pesos: </label>
            <input type="number" className="form-control" id="Cantidad" aria-describedby="Cantidad" placeholder=" $ Cantidad" />
        </div>
        <div className="form-group">
            <label htmlFor="Descripcion">Descripcion del gasto: </label>
            <textarea name="Descripcion" id="Descripcion" cols="35" rows="10">
                Descripcion del gasto
            </textarea>
        </div>
    </form>
</div>