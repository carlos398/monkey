import "./HomePage.css"
import Card from "../../components/Card/Card"

const HomePage = () => {
  const data = [
    {
      id:1,
      type: "Gasto",
      class: "Pago del gym",
      amount: "35.100",
      date: "12/12/2019",
    },

    {
      id:2,
      type: "Ingreso",
      class: "Pago de Inlazz",
      amount: "500.000",
      date: "12/12/2022",
    }
  ]

  return (
    <div className='HomePage'>
        <div className="MovementsContainer">
            {data.map((item ) => {
                return (
                    <Card key={data.id} data={item} />
                )
            })}
        </div>
    </div>
  )
}

export default HomePage